import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';
import { DeleteResult } from 'typeorm';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Role')
@Controller('role')
export class RoleController {
    constructor(private readonly orderService: RoleService) {}

    @Version('1')
    @Post()
    create(@Body() createRoleDto: CreateRoleDto): Promise<Role> {
        return this.orderService.createOne(createRoleDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Role[]> {
        return this.orderService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: number): Promise<Role> {
        return this.orderService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: number, @Body() updateRoleDto: UpdateRoleDto): Promise<Role> {
        return this.orderService.updateOneById(id, updateRoleDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: number): Promise<DeleteResult> {
        return this.orderService.removeOneById(id);
    }
}
