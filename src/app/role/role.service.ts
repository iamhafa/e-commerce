import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RoleService {
    constructor(
        @InjectRepository(Role)
        private orderRepository: Repository<Role>,
    ) {}

    async createOne(createRoleDto: CreateRoleDto): Promise<Role> {
        const newRole = this.orderRepository.create(createRoleDto);
        return this.orderRepository.save(newRole);
    }

    async findAll(): Promise<Role[]> {
        return this.orderRepository.find();
    }

    async findOneById(id: number): Promise<Role> {
        const found = await this.orderRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`role ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: number, updateRoleDto: UpdateRoleDto): Promise<Role> {
        const found = await this.findOneById(id);
        const newData = await this.orderRepository.update(id, updateRoleDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: number): Promise<DeleteResult> {
        return this.orderRepository.delete(id);
    }
}
