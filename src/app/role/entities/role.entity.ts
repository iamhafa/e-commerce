import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { User } from 'src/app/user/entities/user.entity';
import { Permission } from 'src/app/permission/entities/permission.entity';

@Entity()
export class Role {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ unique: true })
    name: string;

    @OneToMany(() => User, (user) => user.roleId)
    users?: User[];

    @OneToMany(() => Permission, (permission) => permission.roleId)
    permissions?: Permission[];
}
