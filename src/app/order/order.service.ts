import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
    ) {}

    async createOne(createOrderDto: CreateOrderDto): Promise<Order> {
        const newOrder = this.orderRepository.create(createOrderDto);
        return this.orderRepository.save(newOrder);
    }

    async findAll(): Promise<Order[]> {
        return this.orderRepository.find();
    }

    async findOneById(id: string): Promise<Order> {
        const found = await this.orderRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`category ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateOrderDto: UpdateOrderDto): Promise<Order> {
        const found = await this.findOneById(id);
        const newData = await this.orderRepository.update(id, updateOrderDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.orderRepository.delete(id);
    }
}
