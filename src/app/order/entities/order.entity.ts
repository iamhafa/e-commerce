import {
    Column,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    Entity,
    ManyToMany,
    ManyToOne,
} from 'typeorm';
import { Product } from 'src/app/product/entities/product.entity';
import { User } from 'src/app/user/entities/user.entity';

@Entity()
export class Order {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('uuid')
    userId: string;

    @Column()
    total: number;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToMany(() => Product, (product) => product.orders)
    products?: Product[];

    @ManyToOne(() => User, (user) => user.orders)
    user: User;
}
