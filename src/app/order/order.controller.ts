import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { DeleteResult } from 'typeorm';

@ApiTags('Order')
@Controller('order')
export class OrderController {
    constructor(private readonly orderService: OrderService) {}

    @Version('1')
    @Post()
    create(@Body() createOrderDto: CreateOrderDto): Promise<Order> {
        return this.orderService.createOne(createOrderDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Order[]> {
        return this.orderService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Order> {
        return this.orderService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto): Promise<Order> {
        return this.orderService.updateOneById(id, updateOrderDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.orderService.removeOneById(id);
    }
}
