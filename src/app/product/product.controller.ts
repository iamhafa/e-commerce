import { DeleteResult } from 'typeorm';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Product } from './entities/product.entity';
import {
    Controller,
    Get,
    Post,
    Body,
    Param,
    Delete,
    Put,
    Version,
    UseInterceptors,
    UploadedFile,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadImage } from 'src/decorator/upload.image';

@ApiTags('Product')
@Controller('product')
export class ProductController {
    constructor(private readonly productService: ProductService) {}

    @Version('1')
    @Post()
    @ApiConsumes('multipart/form-data')
    @UploadImage()
    @UseInterceptors(FileInterceptor('file'))
    uploadFile(@Body() createProductDto: CreateProductDto, @UploadedFile() file: Express.Multer.File) {
        return this.productService.createOne(createProductDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Product[]> {
        return this.productService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Product> {
        return this.productService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto): Promise<Product> {
        return this.productService.updateOneById(id, updateProductDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.productService.removeOneById(id);
    }
}
