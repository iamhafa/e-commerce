import { ApiProperty } from '@nestjs/swagger';

export class CreateProductDto {
    @ApiProperty()
    id: string;

    @ApiProperty()
    name: string;

    @ApiProperty()
    slug: string;

    @ApiProperty()
    desc: string;

    @ApiProperty()
    categoryId: string;

    @ApiProperty()
    price: number;

    @ApiProperty()
    discountId: string;

    @ApiProperty()
    isDeleted: boolean;

    @ApiProperty()
    status: number;
}
