import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Provider } from 'src/app/provider/entities/provider.entity';
import { Category } from 'src/app/category/entities/category.entity';
import { Discount } from 'src/app/discount/entities/discount.entity';
import { Order } from 'src/app/order/entities/order.entity';
import { ProductImage } from 'src/app/product_image/entities/product_image.entity';

@Entity()
export class Product {
    @ApiProperty()
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty()
    @Column()
    name: string;

    @ApiProperty()
    @Column('text', { nullable: true })
    slug: string;

    @ApiProperty()
    @Column('text', { nullable: true })
    desc: string;

    @ApiProperty()
    @Column()
    providerId: string;

    @ApiProperty()
    @Column('uuid')
    categoryId: string;

    @ApiProperty()
    @Column()
    price: number;

    @ApiProperty()
    @Column({ default: 0 })
    inventory: number;

    @ApiProperty()
    @Column('uuid', { nullable: true })
    discountId: string;

    @ApiProperty()
    @Column({ default: false })
    isDeleted: boolean;

    @ApiProperty()
    @Column({ default: 1 })
    status: number;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToOne(() => Provider, (provider) => provider.products)
    provider: Provider;

    @ManyToOne(() => Category, (category) => category.products)
    category: Category;

    @ManyToOne(() => Discount, (discount) => discount.products)
    discount: Discount;

    @ManyToMany(() => Order, (order) => order.products, { cascade: true })
    @JoinTable({
        name: 'order_details',
    })
    orders?: Order[];

    @OneToMany(() => ProductImage, (productImage) => productImage.productId)
    productImages: ProductImage[];
}
