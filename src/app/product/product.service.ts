import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
    ) {}

    async createOne(createProductDto: CreateProductDto): Promise<Product> {
        const newProduct = this.productRepository.create(createProductDto);
        return this.productRepository.save(newProduct);
    }

    async findAll(): Promise<Product[]> {
        return this.productRepository.find({
            relations: {
                category: true,
                discount: true,
                provider: true,
            },
        });
    }

    async findOneById(id: string): Promise<Product> {
        const found = await this.productRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`product ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateProductDto: UpdateProductDto): Promise<Product> {
        const found = await this.findOneById(id);
        const newData = await this.productRepository.update(id, updateProductDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.productRepository.delete(id);
    }
}
