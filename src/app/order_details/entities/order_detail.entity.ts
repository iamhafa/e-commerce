import {
    Column,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    Entity,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from 'typeorm';
import { Product } from 'src/app/product/entities/product.entity';
import { Order } from 'src/app/order/entities/order.entity';

@Entity({ name: 'order_details' })
export class OrderDetail {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @PrimaryColumn()
    productId: string;

    @PrimaryColumn()
    orderId: string;

    @Column()
    quantity: number;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToOne(() => Order, (order) => order.products)
    @JoinColumn([{ name: 'orderId', referencedColumnName: 'id' }])
    orders?: Order[];

    @ManyToOne(() => Product, (product) => product.orders)
    @JoinColumn([{ name: 'productId', referencedColumnName: 'id' }])
    products?: Product[];
}
