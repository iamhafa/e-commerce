import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateOrderDetailDto } from './dto/create-order_detail.dto';
import { UpdateOrderDetailDto } from './dto/update-order_detail.dto';
import { OrderDetail } from './entities/order_detail.entity';

@Injectable()
export class OrderDetailsService {
    constructor(
        @InjectRepository(OrderDetail)
        private orderRepository: Repository<OrderDetail>,
    ) {}

    async createOne(createOrderDetailDto: CreateOrderDetailDto): Promise<OrderDetail> {
        const newOrderDetail = this.orderRepository.create(createOrderDetailDto);
        return this.orderRepository.save(newOrderDetail);
    }

    async findAll(): Promise<OrderDetail[]> {
        return this.orderRepository.find();
    }

    async findOneById(id: string): Promise<OrderDetail> {
        const found = await this.orderRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`order detail ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateOrderDetailDto: UpdateOrderDetailDto): Promise<OrderDetail> {
        const found = await this.findOneById(id);
        const newData = await this.orderRepository.update(id, updateOrderDetailDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.orderRepository.delete(id);
    }
}
