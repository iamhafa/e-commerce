import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { OrderDetailsService } from './order_details.service';
import { CreateOrderDetailDto } from './dto/create-order_detail.dto';
import { UpdateOrderDetailDto } from './dto/update-order_detail.dto';
import { OrderDetail } from './entities/order_detail.entity';
import { DeleteResult } from 'typeorm';

@ApiTags('Order Details')
@Controller('order-details')
export class OrderDetailsController {
    constructor(private readonly discountService: OrderDetailsService) {}

    @Version('1')
    @Post()
    create(@Body() createDiscountDto: CreateOrderDetailDto): Promise<OrderDetail> {
        return this.discountService.createOne(createDiscountDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<OrderDetail[]> {
        return this.discountService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<OrderDetail> {
        return this.discountService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateDiscountDto: UpdateOrderDetailDto): Promise<OrderDetail> {
        return this.discountService.updateOneById(id, updateDiscountDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.discountService.removeOneById(id);
    }
}
