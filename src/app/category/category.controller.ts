import { ApiTags } from '@nestjs/swagger';
import { DeleteResult } from 'typeorm';
import { Category } from './entities/category.entity';
import { Controller, Get, Post, Body, Patch, Param, Delete, Put, Version } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@ApiTags('Category')
@Controller('category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) {}

    @Version('1')
    @Post()
    create(@Body() createCategoryDto: CreateCategoryDto): Promise<Category> {
        return this.categoryService.createOne(createCategoryDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Category[]> {
        return this.categoryService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Category> {
        return this.categoryService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateCategoryDto: UpdateCategoryDto): Promise<Category> {
        return this.categoryService.updateOneById(id, updateCategoryDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.categoryService.removeOneById(id);
    }
}
