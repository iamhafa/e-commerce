import { Repository, DeleteResult } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Category } from './entities/category.entity';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private categoryRepository: Repository<Category>,
    ) {}

    async createOne(createCategoryDto: CreateCategoryDto): Promise<Category> {
        const newCategory = this.categoryRepository.create(createCategoryDto);
        return this.categoryRepository.save(newCategory);
    }

    async findAll(): Promise<Category[]> {
        return this.categoryRepository.find();
    }

    async findOneById(id: string): Promise<Category> {
        const found = await this.categoryRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`category ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateCategoryDto: UpdateCategoryDto): Promise<Category> {
        const found = await this.findOneById(id);
        const newData = await this.categoryRepository.update(id, updateCategoryDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.categoryRepository.delete(id);
    }
}
