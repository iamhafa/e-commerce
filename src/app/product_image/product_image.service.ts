import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateProductImageDto } from './dto/create-product_image.dto';
import { UpdateProductImageDto } from './dto/update-product_image.dto';
import { ProductImage } from './entities/product_image.entity';

@Injectable()
export class ProductImageService {
    constructor(
        @InjectRepository(ProductImage)
        private productImageRepository: Repository<ProductImage>,
    ) {}

    async createOne(createProductImageDto: CreateProductImageDto): Promise<ProductImage> {
        const newProductImage = this.productImageRepository.create(createProductImageDto);
        return this.productImageRepository.save(newProductImage);
    }

    async findAll(): Promise<ProductImage[]> {
        return this.productImageRepository.find();
    }

    async findOneById(id: string): Promise<ProductImage> {
        const found = await this.productImageRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`product image ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateProductImageDto: UpdateProductImageDto): Promise<ProductImage> {
        const found = await this.findOneById(id);
        const newData = await this.productImageRepository.update(id, updateProductImageDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.productImageRepository.delete(id);
    }
}
