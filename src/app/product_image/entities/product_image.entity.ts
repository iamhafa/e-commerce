import { ApiProperty } from '@nestjs/swagger';
import { Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, Entity } from 'typeorm';
import { Product } from 'src/app/product/entities/product.entity';

@Entity()
export class ProductImage {
    @ApiProperty()
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ApiProperty()
    @Column('uuid')
    productId: string;

    @ApiProperty()
    @Column({ type: 'text' })
    imageURL: string;

    @ApiProperty()
    @Column({ nullable: true })
    alt: string;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @ManyToOne(() => Product, (product) => product.productImages)
    product: Product;
}
