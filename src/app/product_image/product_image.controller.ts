import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { ProductImageService } from './product_image.service';
import { CreateProductImageDto } from './dto/create-product_image.dto';
import { UpdateProductImageDto } from './dto/update-product_image.dto';
import { ProductImage } from './entities/product_image.entity';
import { DeleteResult } from 'typeorm';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Product Image')
@Controller('product-image')
export class ProductImageController {
    constructor(private readonly productImageService: ProductImageService) {}

    @Version('1')
    @Post()
    create(@Body() createProductImageDto: CreateProductImageDto): Promise<ProductImage> {
        return this.productImageService.createOne(createProductImageDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<ProductImage[]> {
        return this.productImageService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<ProductImage> {
        return this.productImageService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateProductImageDto: UpdateProductImageDto): Promise<ProductImage> {
        return this.productImageService.updateOneById(id, updateProductImageDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.productImageService.removeOneById(id);
    }
}
