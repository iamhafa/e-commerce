import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { ProviderService } from './provider.service';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDto } from './dto/update-provider.dto';
import { Provider } from './entities/provider.entity';
import { DeleteResult } from 'typeorm';

@ApiTags('Provider')
@Controller('provider')
export class ProviderController {
    constructor(private readonly orderService: ProviderService) {}

    @Version('1')
    @Post()
    create(@Body() createProviderDto: CreateProviderDto): Promise<Provider> {
        return this.orderService.createOne(createProviderDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Provider[]> {
        return this.orderService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Provider> {
        return this.orderService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateProviderDto: UpdateProviderDto): Promise<Provider> {
        return this.orderService.updateOneById(id, updateProviderDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.orderService.removeOneById(id);
    }
}
