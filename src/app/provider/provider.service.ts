import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateProviderDto } from './dto/create-provider.dto';
import { UpdateProviderDto } from './dto/update-provider.dto';
import { Provider } from './entities/provider.entity';

@Injectable()
export class ProviderService {
    constructor(
        @InjectRepository(Provider)
        private providerRepository: Repository<Provider>,
    ) {}

    async createOne(createProviderDto: CreateProviderDto): Promise<Provider> {
        const newProvider = this.providerRepository.create(createProviderDto);
        return this.providerRepository.save(newProvider);
    }

    async findAll(): Promise<Provider[]> {
        return this.providerRepository.find();
    }

    async findOneById(id: string): Promise<Provider> {
        const found = await this.providerRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`provider ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateProviderDto: UpdateProviderDto): Promise<Provider> {
        const found = await this.findOneById(id);
        const newData = await this.providerRepository.update(id, updateProviderDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.providerRepository.delete(id);
    }
}
