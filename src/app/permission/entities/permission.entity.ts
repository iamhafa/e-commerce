import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Role } from 'src/app/role/entities/role.entity';
import { EMethod } from 'src/constants/method.enum';

@Entity()
export class Permission {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    routes: string;

    @Column({ type: 'enum', enum: EMethod, default: EMethod.GET })
    method: EMethod;

    @Column()
    roleId: number;

    @ManyToOne(() => Role, (role) => role.permissions)
    role: Role;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;
}
