import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { PermissionService } from './permission.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { Permission } from './entities/permission.entity';
import { DeleteResult } from 'typeorm';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Permission')
@Controller('permission')
export class PermissionController {
    constructor(private readonly permissionService: PermissionService) {}

    @Version('1')
    @Post()
    create(@Body() createPermissionDto: CreatePermissionDto): Promise<Permission> {
        return this.permissionService.createOne(createPermissionDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Permission[]> {
        return this.permissionService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Permission> {
        return this.permissionService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updatePermissionDto: UpdatePermissionDto): Promise<Permission> {
        return this.permissionService.updateOneById(id, updatePermissionDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.permissionService.removeOneById(id);
    }
}
