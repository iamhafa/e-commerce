import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { Permission } from './entities/permission.entity';

@Injectable()
export class PermissionService {
    constructor(
        @InjectRepository(Permission)
        private permissionRepository: Repository<Permission>,
    ) {}

    async createOne(createPermissionDto: CreatePermissionDto): Promise<Permission> {
        const newPermission = this.permissionRepository.create(createPermissionDto);
        return this.permissionRepository.save(newPermission);
    }

    async findAll(): Promise<Permission[]> {
        return this.permissionRepository.find();
    }

    async findOneById(id: string): Promise<Permission> {
        const found = await this.permissionRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`permission ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updatePermissionDto: UpdatePermissionDto): Promise<Permission> {
        const found = await this.findOneById(id);
        const newData = await this.permissionRepository.update(id, updatePermissionDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.permissionRepository.delete(id);
    }
}
