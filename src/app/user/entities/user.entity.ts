import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { Order } from 'src/app/order/entities/order.entity';
import { Role } from 'src/app/role/entities/role.entity';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ unique: true })
    username: string;

    @Column()
    password: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column('number', { nullable: true })
    roleId: string;

    @Column('date')
    DOB: Date;

    @Column()
    phone: string;

    @Column({ default: false })
    isDeleted: boolean;

    @Column({ default: 1 })
    status: number;

    @CreateDateColumn()
    createdDate: Date;

    @UpdateDateColumn()
    updatedDate: Date;

    @OneToMany(() => Order, (order) => order.userId)
    orders: Order[];

    @ManyToOne(() => Role, (role) => role.users)
    role: Role;
}
