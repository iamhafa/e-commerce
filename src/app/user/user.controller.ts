import { ApiTags } from '@nestjs/swagger';
import { DeleteResult } from 'typeorm';
import { User } from './entities/user.entity';
import { Controller, Get, Post, Body, Param, Delete, Put, Version } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@ApiTags('User')
@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}

    @Version('1')
    @Post()
    create(@Body() createUserDto: CreateUserDto): Promise<User> {
        return this.userService.createUser(createUserDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<User[]> {
        return this.userService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOneById(@Param('id') id: string): Promise<User> {
        return this.userService.findOneById(id);
    }

    // @Version('1')
    // @Get(':username')
    // findOneByUsername(@Param('username') username: string): Promise<User> {
    //     return this.userService.findOneByUsername(username);
    // }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto): Promise<User> {
        return this.userService.updateUserById(id, updateUserDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.userService.removeUserById(id);
    }
}
