import { User } from './entities/user.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { DeleteResult, Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {}

    async createUser(createUserDto: CreateUserDto): Promise<User> {
        const newUser = this.userRepository.create(createUserDto);
        return this.userRepository.save(newUser);
    }

    async findAll(): Promise<User[]> {
        return this.userRepository.find();
    }

    async findOneById(id: string): Promise<User> {
        const found = await this.userRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`user id ${id} not found`);
        }
        return found;
    }

    async findOneByUsername(username: string): Promise<User> {
        const found = await this.userRepository.findOneBy({ username });

        if (!found) {
            throw new NotFoundException(`user name ${username} not found`);
        }
        return found;
    }

    async updateUserById(id: string, updateUserDto: UpdateUserDto): Promise<User> {
        const found = await this.findOneById(id);
        const newData = await this.userRepository.update(id, updateUserDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeUserById(id: string): Promise<DeleteResult> {
        return this.userRepository.delete(id);
    }
}
