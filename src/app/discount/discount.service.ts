import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CreateDiscountDto } from './dto/create-discount.dto';
import { UpdateDiscountDto } from './dto/update-discount.dto';
import { Discount } from './entities/discount.entity';

@Injectable()
export class DiscountService {
    constructor(
        @InjectRepository(Discount)
        private orderRepository: Repository<Discount>,
    ) {}

    async createOne(createDiscountDto: CreateDiscountDto): Promise<Discount> {
        const newDiscount = this.orderRepository.create(createDiscountDto);
        return this.orderRepository.save(newDiscount);
    }

    async findAll(): Promise<Discount[]> {
        return this.orderRepository.find();
    }

    async findOneById(id: string): Promise<Discount> {
        const found = await this.orderRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`discount ${id} not found`);
        }
        return found;
    }

    async updateOneById(id: string, updateDiscountDto: UpdateDiscountDto): Promise<Discount> {
        const found = await this.findOneById(id);
        const newData = await this.orderRepository.update(id, updateDiscountDto);

        if (newData.affected === 1) return await this.findOneById(id);
    }

    async removeOneById(id: string): Promise<DeleteResult> {
        return this.orderRepository.delete(id);
    }
}
