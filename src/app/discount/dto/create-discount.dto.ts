import { ApiProperty } from '@nestjs/swagger';

export class CreateDiscountDto {
    @ApiProperty()
    name: string;

    @ApiProperty()
    discountPercent: number;

    @ApiProperty()
    isActive: boolean;
}
