import { ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, Body, Patch, Param, Delete, Version, Put } from '@nestjs/common';
import { DiscountService } from './discount.service';
import { CreateDiscountDto } from './dto/create-discount.dto';
import { UpdateDiscountDto } from './dto/update-discount.dto';
import { Discount } from './entities/discount.entity';
import { DeleteResult } from 'typeorm';

@ApiTags('Discount')
@Controller('discount')
export class DiscountController {
    constructor(private readonly discountService: DiscountService) {}

    @Version('1')
    @Post()
    create(@Body() createDiscountDto: CreateDiscountDto): Promise<Discount> {
        return this.discountService.createOne(createDiscountDto);
    }

    @Version('1')
    @Get()
    findAll(): Promise<Discount[]> {
        return this.discountService.findAll();
    }

    @Version('1')
    @Get(':id')
    findOne(@Param('id') id: string): Promise<Discount> {
        return this.discountService.findOneById(id);
    }

    @Version('1')
    @Put(':id')
    update(@Param('id') id: string, @Body() updateDiscountDto: UpdateDiscountDto): Promise<Discount> {
        return this.discountService.updateOneById(id, updateDiscountDto);
    }

    @Version('1')
    @Delete(':id')
    remove(@Param('id') id: string): Promise<DeleteResult> {
        return this.discountService.removeOneById(id);
    }
}
