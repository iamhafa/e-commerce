import { Body, Controller, Post, Request } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { UserLoginDto } from './dto/login.dto';

@ApiTags('Authen')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('login')
    // login(@Body() userLoginDto: UserLoginDto): any {
    //     const { username, password } = userLoginDto;

    //     return this.authService.validateUser(username, password);
    // }
    login(@Request() req) {
        return req.user;
    }
}
