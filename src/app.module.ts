import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { dataSourceOptions } from './config/data-source';
import { UserModule } from './app/user/user.module';
import { ProductModule } from './app/product/product.module';
import { CategoryModule } from './app/category/category.module';
import { DiscountModule } from './app/discount/discount.module';
import { OrderModule } from './app/order/order.module';
import { OrderDetailsModule } from './app/order_details/order_details.module';
import { ProviderModule } from './app/provider/provider.module';
import { RoleModule } from './app/role/role.module';
import { ProductImageModule } from './app/product_image/product_image.module';
import { AuthModule } from './auth/auth.module';
import { PermissionModule } from './app/permission/permission.module';
import { ConfigModule } from '@nestjs/config';

@Module({
    imports: [
        TypeOrmModule.forRoot(dataSourceOptions),
        ConfigModule.forRoot({ isGlobal: true }),
        AuthModule,
        UserModule,
        ProductModule,
        CategoryModule,
        DiscountModule,
        OrderModule,
        OrderDetailsModule,
        ProviderModule,
        RoleModule,
        ProductImageModule,
        PermissionModule,
    ],
})
export class AppModule {}
