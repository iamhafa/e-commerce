import { DataSource, DataSourceOptions } from 'typeorm';

export const dataSourceOptions: DataSourceOptions = {
    type: 'postgres',
    host: 'localhost',
    username: 'hafa',
    password: '78781227',
    database: 'nest',
    // link to every entities in **dist**
    entities: ['dist/app/**/entities/*.js'],
    synchronize: true,
};

const dataSource = new DataSource(dataSourceOptions);

export default dataSource;
