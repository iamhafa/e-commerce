import helmet from 'helmet';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { VersioningType } from '@nestjs/common';

(async (): Promise<void> => {
    const app = await NestFactory.create(AppModule, { cors: { origin: '*' } });
    app.enableVersioning({ type: VersioningType.URI });
    // app.use(helmet());
    app.setGlobalPrefix('api', { exclude: ['auth/login'] });

    const swaggerOptions = new DocumentBuilder()
        .setTitle('Practices API')
        .setVersion('1.0')
        .setDescription('This is full API')
        .build();
    const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('docs', app, swaggerDocument);

    await app.listen(process.env.PORT);
})();
